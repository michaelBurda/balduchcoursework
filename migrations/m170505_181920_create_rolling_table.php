<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rolling`.
 */
class m170505_181920_create_rolling_table extends Migration
{
    protected $tn_rolling = '{{%rolling}}';
    protected $tn_disc = '{{%disc}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tn_rolling, [
            'id'            => $this->primaryKey(),
            'disc_id'       => $this->integer(),
            'issuanceDate'  => $this->date(),
            'returnDate'    => $this->date(),
            'earnedSum'     => $this->double()
        ]);

        $this->addForeignKey(
            'FK_ROLLING_DISC_ID',
            $this->tn_rolling,
            'disc_id',
            $this->tn_disc,
            'id',
            'NO ACTION',
            'NO ACTION'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('FK_ROLLING_DISC_ID', $this->tn_rolling);

        $this->dropTable($this->tn_rolling);
    }
}
