<?php

use yii\db\Migration;

/**
 * Handles the creation of table `market_place`.
 */
class m170505_173700_create_market_place_table extends Migration
{
    protected $tn_market_place = '{{%market_place}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->tn_market_place, [
            'id'        => $this->primaryKey(),
            'name'      => $this->string(),
            'address'   => $this->string(),
            'square'    => $this->double()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tn_market_place);
    }
}
