<?php

use yii\db\Migration;

/**
 * Handles the creation of table `movie`.
 */
class m170504_205936_create_movie_table extends Migration
{
    protected $tn_movie = '{{%movie}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->tn_movie, [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(),
            'genre_id'      => $this->integer(),
            'releaseDate'   => $this->date()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tn_movie);
    }
}
