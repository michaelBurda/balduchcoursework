<?php

use yii\db\Migration;

/**
 * Handles the creation of table `genre`.
 */
class m170504_212014_create_genre_table extends Migration
{
    protected $tn_genre = '{{%genre}}';
    protected $tn_movie = '{{%movie}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->tn_genre, [
            'id'    => $this->primaryKey(),
            'name'  => $this->string()
        ]);

        $this->addForeignKey(
            'FK_MOVIE_GENRE_ID',
            $this->tn_movie,
            'genre_id',
            $this->tn_genre,
            'id',
            'NO ACTION',
            'NO ACTION'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_MOVIE_GENRE_ID', $this->tn_movie);

        $this->dropTable($this->tn_genre);
    }
}
