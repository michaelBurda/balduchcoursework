<?php

use yii\db\Migration;

/**
 * Handles the creation of table `disc`.
 */
class m170505_181222_create_disc_table extends Migration
{
    protected $tn_disc = '{{%disc}}';
    protected $tn_market_place = '{{%market_place}}';
    protected $tn_movie = '{{%movie}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tn_disc, [
            'id'                => $this->primaryKey(),
            'movie_id'          => $this->integer(),
            'market_place_id'   => $this->integer(),
            'pricePerDay'       => $this->double()
        ]);

        $this->addForeignKey(
            'FK_DISC_MOVIE_ID',
            $this->tn_disc,
            'movie_id',
            $this->tn_movie,
            'id',
            'NO ACTION',
            'NO ACTION'
        );

        $this->addForeignKey(
            'FK_DISC_MARKET_PLACE_ID',
            $this->tn_disc,
            'market_place_id',
            $this->tn_market_place,
            'id',
            'NO ACTION',
            'NO ACTION'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('FK_DISC_MOVIE_ID', $this->tn_disc);
        $this->dropForeignKey('FK_DISC_MARKET_PLACE_ID', $this->tn_disc);

        $this->dropTable($this->tn_disc);
    }
}
