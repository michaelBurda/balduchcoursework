<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\rolling\models\search\RollingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rollings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rolling-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Rolling', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'disc_id',
            'issuanceDate',
            'returnDate',
            'earnedSum',

            [
                'class'     => 'yii\grid\ActionColumn',
                'template'  => $actionButtonsTemplate,
            ],
        ],
    ]); ?>
</div>
