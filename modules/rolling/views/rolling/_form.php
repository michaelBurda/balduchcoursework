<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\rolling\models\Rolling */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rolling-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'disc_id')->widget(Select2::className(), [
        'name' => 'Disc',
        'data' => ArrayHelper::map($discs, 'id', 'movieAsStringWithID'),
        'size' => Select2::MEDIUM,
        'options' => ['placeholder' => '--select disc--', 'multiple' => false],
        'pluginOptions' => ['allowClear' => true],
    ]);?>

    <?= $form->field($model, 'issuanceDate')->textInput() ?>

    <?= $form->field($model, 'returnDate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
