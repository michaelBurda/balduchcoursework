<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\rolling\models\search\RollingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rolling-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'disc_id') ?>

    <?= $form->field($model, 'issuanceDate') ?>

    <?= $form->field($model, 'returnDate') ?>

    <?= $form->field($model, 'earnedSum') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
