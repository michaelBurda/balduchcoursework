<?php

namespace app\modules\rolling\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\rolling\models\Rolling;

/**
 * RollingSearch represents the model behind the search form of `app\modules\rolling\models\Rolling`.
 */
class RollingSearch extends Rolling
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'disc_id'], 'integer'],
            [['issuanceDate', 'returnDate', 'earnedSum'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rolling::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'disc_id' => $this->disc_id,
            'issuanceDate' => $this->issuanceDate,
            'returnDate' => $this->returnDate,
        ]);

        if (!empty($this->earnedSum)) {
            if ($c = $this->isComparable($this->earnedSum)) {
                $query->andWhere([$c['condition'], 'earnedSum', $c['value']]);
            } else {
                $query->andWhere(['earnedSum' => $this->earnedSum]);
            }
        }

        return $dataProvider;
    }

    // check, is string contain comparable symbols ('>=', '<=', '>', '<')
    // and return false, or ['condition' => '...', 'value' => '...']
    private function isComparable($string) {
        $conditions = ['>=', '<=', '>', '<'];
        foreach ($conditions as $condition) {
            if (strpos($string, $condition) === 0) {
                return [
                    'condition' => $condition,
                    'value' => str_replace($condition, '', $string)
                ];
            }
        }
        return false;
    }
}
