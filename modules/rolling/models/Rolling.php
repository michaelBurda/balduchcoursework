<?php

namespace app\modules\rolling\models;

use app\modules\disc\models\Disc;
use Yii;

/**
 * This is the model class for table "{{%rolling}}".
 *
 * @property int $id
 * @property int $disc_id
 * @property string $issuanceDate
 * @property string $returnDate
 * @property double $earnedSum
 */
class Rolling extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rolling}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['disc_id'], 'integer'],
            [['issuanceDate', 'returnDate'], 'safe'],
            [['earnedSum'], 'number'],
        ];
    }

    public function fields()
    {
        return [
            'movieNameWithDiscID'
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                    => 'ID',
            'disc_id'               => 'Disc ID',
            'issuanceDate'          => 'Issuance Date',
            'returnDate'            => 'Return Date',
            'earnedSum'             => 'Earned Sum',
            'movieNameWithDiscID'   => 'Disc'
        ];
    }

    public function getMovieNameWithDiscID()
    {
        return (Disc::findOne($this->disc_id) === null) ? null : Disc::findOne($this->disc_id)->movieAsStringWithID;
    }

    public function beforeSave($insert)
    {
        if ($this->issuanceDate && $this->returnDate) {
            if (strtotime($this->issuanceDate) > strtotime($this->returnDate)) {
                return false;
            }

            $interval   = date_diff(date_create($this->issuanceDate), date_create($this->returnDate));
            $daysDiff   = intval($interval->format('%a')) + 1; // if diff is 0, count it as 1 day

            $this->earnedSum = $daysDiff * Disc::findOne($this->disc_id)->pricePerDay;
        }

        return true;
    }
}
