<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\disc\models\Disc */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="disc-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'movie_id')->widget(Select2::className(), [
        'name' => 'Movie',
        'data' => ArrayHelper::map($movies, 'id', 'name'),
        'size' => Select2::MEDIUM,
        'options' => ['placeholder' => '--select movie--', 'multiple' => false],
        'pluginOptions' => ['allowClear' => true],
    ]);?>

    <?= $form->field($model, 'market_place_id')->widget(Select2::className(), [
        'name' => 'Market Place',
        'data' => ArrayHelper::map($marketPlaces, 'id', 'name'),
        'size' => Select2::MEDIUM,
        'options' => ['placeholder' => '--select market place--', 'multiple' => false],
        'pluginOptions' => ['allowClear' => true],
    ]);?>

    <?= $form->field($model, 'pricePerDay')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
