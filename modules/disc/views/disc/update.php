<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\disc\models\Disc */

$this->title = 'Update Disc: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Discs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="disc-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'         => $model,
        'movies'        => $movies,
        'marketPlaces'  => $marketPlaces
    ]) ?>

</div>
