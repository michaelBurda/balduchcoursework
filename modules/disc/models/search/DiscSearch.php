<?php

namespace app\modules\disc\models\search;

use app\modules\marketPlace\models\MarketPlace;
use app\modules\movie\models\Movie;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\disc\models\Disc;

/**
 * DiscSearch represents the model behind the search form of `app\modules\disc\models\Disc`.
 */
class DiscSearch extends Disc
{
    public $movieAsString;
    public $marketPlaceAsString;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'movie_id', 'market_place_id'], 'integer'],
            [['movieAsString', 'marketPlaceAsString', 'pricePerDay'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tn_disc            = Disc::tableName();
        $tn_movie           = Movie::tableName();
        $tn_market_place    = MarketPlace::tableName();

        $query = Disc::find()
            ->from("$tn_disc as d")
            ->leftJoin("$tn_movie as m", 'm.id = d.movie_id')
            ->leftJoin("$tn_market_place as mp", 'mp.id = d.market_place_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc'   => ['d.id' => SORT_ASC],
                    'desc'  => ['d.id' => SORT_DESC]
                ],
                'movieAsString' => [
                    'asc'   => ['m.name' => SORT_ASC],
                    'desc'  => ['m.name' => SORT_DESC]
                ],
                'marketPlaceAsString' => [
                    'asc'   => ['mp.name' => SORT_ASC],
                    'desc'  => ['mp.name' => SORT_DESC]
                ],
                'pricePerDay' => [
                    'asc'   => ['d.pricePerDay' => SORT_ASC],
                    'desc'  => ['d.pricePerDay' => SORT_DESC]
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if (!empty($this->id)) {
            $query->andFilterWhere(['d.id' => $this->id]);
        }

        if (!empty($this->movieAsString)) {
            $query->andFilterWhere(['like', 'm.name', $this->movieAsString]);
        }

        if (!empty($this->marketPlaceAsString)) {
            $query->andFilterWhere(['like', 'mp.name', $this->marketPlaceAsString]);
        }

        if (!empty($this->pricePerDay)) {
            if ($c = $this->isComparable($this->pricePerDay)) {
                $query->andWhere([$c['condition'], 'd.pricePerDay', $c['value']]);
            } else {
                $query->andWhere(['d.pricePerDay' => $this->pricePerDay]);
            }
        }

        return $dataProvider;
    }

    // check, is string contain comparable symbols ('>=', '<=', '>', '<')
    // and return false, or ['condition' => '...', 'value' => '...']
    private function isComparable($string) {
        $conditions = ['>=', '<=', '>', '<'];
        foreach ($conditions as $condition) {
            if (strpos($string, $condition) === 0) {
                return [
                    'condition' => $condition,
                    'value' => str_replace($condition, '', $string)
                ];
            }
        }
        return false;
    }
}
