<?php

namespace app\modules\disc\models;

use app\modules\marketPlace\models\MarketPlace;
use app\modules\movie\models\Movie;
use Yii;

/**
 * This is the model class for table "{{%disc}}".
 *
 * @property int $id
 * @property int $movie_id
 * @property int $market_place_id
 * @property double $pricePerDay
 */
class Disc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%disc}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['movie_id', 'market_place_id'], 'integer'],
            [['pricePerDay'], 'number'],
        ];
    }

    public function fields()
    {
        return [
            'movieAsString',
            'movieAsStringWithID',
            'marketPlaceAsString'
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                    => 'ID',
            'movie_id'              => 'Movie',
            'market_place_id'       => 'Market Place',
            'pricePerDay'           => 'Price Per Day',
            'movieAsString'         => 'Movie',
            'movieAsStringWithID'   => 'Movie',
            'marketPlaceAsString'   => 'Market place'
        ];
    }

    public function getMovie()
    {
        return $this->hasOne(Movie::className(), ['id' => 'movie_id']);
    }

    public function getMovieAsString()
    {
        return ($this->movie === null) ? null : $this->movie->name;
    }

    public function getMovieAsStringWithID()
    {
        return ($this->movie === null) ? null : '#' . $this->id . ' (' . $this->movie->name . ')';
    }

    public function getMarketPlace()
    {
        return $this->hasOne(MarketPlace::className(), ['id' => 'market_place_id']);
    }

    public function getMarketPlaceAsString()
    {
        return ($this->marketPlace === null) ? null : $this->marketPlace->name;
    }
}
