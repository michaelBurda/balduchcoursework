<?php

namespace app\modules\marketPlace\models;

use Yii;

/**
 * This is the model class for table "{{%market_place}}".
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property double $square
 */
class MarketPlace extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%market_place}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['square'], 'number'],
            [['name', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'name'      => 'Name',
            'address'   => 'Address',
            'square'    => 'Square',
        ];
    }
}
