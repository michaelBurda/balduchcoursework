<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\marketPlace\models\search\MarketPlaceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Market Places';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="market-place-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Market Place', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'address',
            'square',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
