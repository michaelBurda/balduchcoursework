<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\marketPlace\models\MarketPlace */

$this->title = 'Create Market Place';
$this->params['breadcrumbs'][] = ['label' => 'Market Places', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="market-place-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
