<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\movie\models\Movie */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="movie-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'genre_id')->widget(Select2::className(), [
        'name' => 'Genre',
        'data' => ArrayHelper::map($genres, 'id', 'name'),
        'size' => Select2::MEDIUM,
        'options' => ['placeholder' => '--select genre--', 'multiple' => false],
        'pluginOptions' => ['allowClear' => true],
    ]);?>

    <?= $form->field($model, 'releaseDate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
