<?php

namespace app\modules\movie\models\search;

use app\modules\genre\models\Genre;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\movie\models\Movie;

/**
 * MovieSearch represents the model behind the search form of `app\modules\movie\models\Movie`.
 */
class MovieSearch extends Movie
{
    public $genreAsString;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'genre_id'], 'integer'],
            [['name', 'releaseDate', 'genreAsString'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tn_movie = Movie::tableName();
        $tn_genre = Genre::tableName();

        $query = Movie::find()
            ->from("$tn_movie as m")
            ->leftJoin("$tn_genre as g", 'g.id = m.genre_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc'   => ['m.id' => SORT_ASC],
                    'desc'  => ['m.id' => SORT_DESC]
                ],
                'name' => [
                    'asc'   => ['m.name' => SORT_ASC],
                    'desc'  => ['m.name' => SORT_DESC]
                ],
                'genreAsString' => [
                    'asc'   => ['g.name' => SORT_ASC],
                    'desc'  => ['g.name' => SORT_DESC]
                ],
                'releaseDate' => [
                    'asc'   => ['m.releaseDate' => SORT_ASC],
                    'desc'  => ['m.releaseDate' => SORT_DESC]
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if (!empty($this->id)) {
            $query->andFilterWhere(['m.id' => $this->id]);
        }

        if (!empty($this->name)) {
            $query->andFilterWhere(['like', 'm.name', $this->name]);
        }

        if (!empty($this->genreAsString)) {
            $query->andFilterWhere(['like', 'g.name', $this->genreAsString]);
        }

        if (!empty($this->releaseDate)) {
            $query->andFilterWhere(['like', 'm.releaseDate', $this->releaseDate]);
        }

        return $dataProvider;
    }
}
