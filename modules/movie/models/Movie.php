<?php

namespace app\modules\movie\models;

use app\modules\genre\models\Genre;
use Yii;

/**
 * This is the model class for table "{{%movie}}".
 *
 * @property int $id
 * @property string $name
 * @property int $genre_id
 * @property string $releaseDate
 */
class Movie extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%movie}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['genre_id'], 'integer'],
            [['releaseDate'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function fields()
    {
        return [
            'genreAsString',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'name'          => 'Name',
            'genre_id'      => 'Genre',
            'releaseDate'   => 'Release Date',
            'genreAsString' => 'Genre'
        ];
    }

    public function getGenre()
    {
        return $this->hasOne(Genre::className(), ['id' => 'genre_id']);
    }

    public function getGenreAsString() {
        return ($this->genre === null) ? null : $this->genre->name;
    }
}
